﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GriffithTreaserFinder.Models;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using GriffithTreaserFinder.DataObjects;
using Autofac.Core;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Auth;

namespace GriffithTreaserFinder.Controllers
{
    public class ProfilesController : ApiController
    {

       public ApiServices Services { get; set; } //need ApiSErvice To work on Mobile services 

        private GriffithDataModel db = new GriffithDataModel();

     //   [AuthorizeLevel(AuthorizationLevel.Admin)]
        // GET: api/Profiles
        public IQueryable<Profile> GetProfiles()
        {

            return db.Profiles;
        }


        [AuthorizeLevel(AuthorizationLevel.User)]
        // Post: api/Profiles/5
        [Route("api/UserProfile")]
        [ResponseType(typeof(ProfileDto))]
        public async Task<IHttpActionResult> PostUserProfile()
        {
            var user = User as ServiceUser;
        //User Id Return From User always have Custome: Prefix ,have to cut it out of string to get the Actual Id for Query 
            string userId = user.Id.Split(':')[1].Trim();
            int id = Convert.ToInt32(userId);
            Profile profile = await db.Profiles.Where(e => e.Account_Id == id).FirstOrDefaultAsync();
  
            if (profile == null)
            {
                return NotFound();
            }

            ProfileDto profileObject = new ProfileDto()
            {
                profileId=profile.Id,
                FirstName = profile.FirstName,
                LastName  = profile.LastName,
                MiddleName= profile.MiddleName,
                BirthDay  = profile.BirthDay,
                Major     = profile.Major,
                EntryYear = profile.EntryYear,
                Sex       = profile.Sex,
                Campus    = profile.Campus
            };

            return Ok(profileObject);
        }

        // PUT: api/Profiles/5
        [AuthorizeLevel(AuthorizationLevel.User)]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProfile(ProfileDto profileDto)
        {
            //Get User Id 
            var user = User as ServiceUser;
            //User Id Return From User always have Custome: Prefix ,have to cut it out of string to get the Actual Id for Query 
            string userId = user.Id.Split(':')[1].Trim();
            int id = Convert.ToInt32(userId);
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Profile profile = await db.Profiles.Where(e => e.Account_Id == id).FirstOrDefaultAsync();

            profile.FirstName   = profileDto.FirstName;
            profile.MiddleName  = profileDto.MiddleName;
            profile.LastName    = profileDto.LastName;
            profile.BirthDay    = profileDto.BirthDay;
            profile.Major       = profileDto.Major;
            profile.EntryYear   = profileDto.EntryYear;
            profile.Sex         = profileDto.Sex;
            profile.Campus      = profileDto.Campus;
            db.Entry(profile).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProfileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.Accepted);
        }

        // POST: api/Profiles
        [ResponseType(typeof(Profile))]
        public async Task<IHttpActionResult> PostProfile(Profile profile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Profiles.Add(profile);
            await db.SaveChangesAsync();
            return Ok(HttpStatusCode.Created);
        }
         [AuthorizeLevel(AuthorizationLevel.Admin)]
        // DELETE: api/Profiles/5
        [ResponseType(typeof(Profile))]
        public async Task<IHttpActionResult> DeleteProfile(int id)
        {
            Profile profile = await db.Profiles.FindAsync(id);
            if (profile == null)
            {
                return NotFound();
            }

            db.Profiles.Remove(profile);
            await db.SaveChangesAsync();
            return Ok(profile);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private bool ProfileExists(int id)
        {
            return db.Profiles.Count(e => e.Id == id) > 0;
        }
    }
}