﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.Controllers
{
    public class PagingResualt<T>
    {
        public class PageInfo
        {
            public int PageNo { get; set; }
            public int PageSize { get; set; }
            public int PageCount { get; set; }
            public long TotalRecourdCount { get; set; }
        }
        public List<T> Data { get; set; }
        public PageInfo Paging { get; private set; }

        public PagingResualt(IEnumerable<T> items, int pageNo, int pageSize, long totalRecordCount)
        {


            Data = new List<T>(items);
            Paging = new PageInfo
            {
                PageNo = pageNo,
                PageSize = pageSize,
                TotalRecourdCount = totalRecordCount,
                PageCount = totalRecordCount > 0 ? (int)Math.Ceiling(totalRecordCount / (double)pageSize) : 0

            };


        }
    }
}