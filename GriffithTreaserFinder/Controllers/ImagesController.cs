﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using GriffithTreaserFinder.DataObjects;
using Autofac.Core;
using GriffithTreaserFinder.Models;
using GriffithTreaserFinder.Models.BlobStorage;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace GriffithTreaserFinder.Controllers
{
    public class ImagesController : ApiController
    {

        private GriffithDataModel db = new GriffithDataModel();
        public ApiServices Services { get; set; } //need ApiSErvice To work on Mobile services

        /*
         * POST::Method to create Blob container and save to databasess
         */
        [AuthorizeLevel(AuthorizationLevel.User)]
        public async Task<IHttpActionResult> PostImage()
        {

            var user = User as ServiceUser;
            //User Id Return From User always have Custome: Prefix ,have to cut it out of string to get the Actual Id for Query 
            string userId = user.Id.Split(':')[1].Trim();
            int id = Convert.ToInt32(userId);
              //fid user profile by therer accessToken And account Id 
            Profile profile =  db.Profiles.Where(e => e.Account_Id == id).FirstOrDefault();
            if (profile == null)
            {
                return BadRequest("Can't Find the profile");
            }
              //check if has ResourcesName create container then save it to DB
            UserImageDto item = new UserImageDto();
                item.ContainerName = "griffihtimagedata";
                item.ResourcesName = Guid.NewGuid().ToString()+".jpg";
                BlobStorageCustomHandler blobHandler = new BlobStorageCustomHandler();
                UserImage image = await blobHandler.returnImage(item);
                image.CreatedAt = System.DateTimeOffset.UtcNow;        
                image.ProfileId = profile.Id;
                image.ProfilePic = false;
                db.UserImages.Add(image);
                db.SaveChanges();
                //Retunr string with x-ms-date format 
                BlobImage infoReturn = new BlobImage() {
                    UtcDate = image.CreatedAt.ToString("R"),
                    Sas = image.imageUri+image.SasQueryString
                };
                return Ok(infoReturn);
        }

        /*
         * Get: Mthod to Return images from Blob
         * Return type: List of images Uris 
         */
        [AuthorizeLevel(AuthorizationLevel.User)]
        public IHttpActionResult GetImages()
        {
            var user = User as ServiceUser;
            //User Id Return From User always have Custome: Prefix ,have to cut it out of string to get the Actual Id for Query 
            string userId = user.Id.Split(':')[1].Trim();
            int id = Convert.ToInt32(userId);
            //fid user profile by therer accessToken And account Id 
            var images =db.UserImages.Where(e => e.Profile.Account_Id == id);
           //  var images = db.UserImages.Where(ee => ee.ProfileId == Id);
           if (images.Count() == 0) {
               return NotFound();
           }
           List<string> imagesUris = new List<string>();
           foreach (var i in images) {
              imagesUris.Add(i.imageUri);
           }
            return Ok(imagesUris);
        }



        /*
         * change the bool value for the Profile picture to 1
         * I think best way is to create a Triger for it on SQL ?????
         * 
         */
        [Route("api/changeDefault")]
        public IHttpActionResult PostChangeDefault()
        {


            return Ok();

        }

    }
}
