﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GriffithTreaserFinder.Models;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using GriffithTreaserFinder.DataObjects;
using Microsoft.WindowsAzure.Mobile.Service;

namespace GriffithTreaserFinder.Controllers
{
    public class PostsController : ApiController
    {
        private GriffithDataModel db = new GriffithDataModel();
        public ApiServices Services { get; set; } //need ApiSErvice To work on Mobile services


        // GET: api/Posts
        /*
         * Need to caculat5e dateTime for how many days befor you want and do the query 
         * DateTimeOffset.add(-Days)
         */
        public IQueryable<Post> GetPosts()
        {
            return db.Posts.Include(e => e.Comments)
                .OrderByDescending(xx=>xx.Id);
        }


        /*
         * Return all the comments belong to post 
         
         */
        [Route("api/getComments")]
        public IQueryable<Comment> getComments(int Id)
        {
            return db.Comments.Where(e => e.PostId == Id)
                .OrderByDescending(x=>x.Id);
        }



        /*
         *  POST: api/Posts
         * Input PostDto 
         */
        [AuthorizeLevel(AuthorizationLevel.User)]
        public async Task<IHttpActionResult> PostPost(PostDto postDto)
        {

            if (postDto.Description.Length==0)
            {
                return BadRequest("Description Empty");
            }
            var user = User as ServiceUser;
            //User Id Return From User always have Custome: Prefix ,have to cut it out of string to get the Actual Id for Query 
            string userId = user.Id.Split(':')[1].Trim();
            int accountId = Convert.ToInt32(userId);
              //Create post 
            Post post = new Post();
            post.Title = postDto.Title;
            post.Description = postDto.Description;
            post.CreatedAt = System.DateTimeOffset.UtcNow;
          
             post.AccountId=accountId;
            db.Posts.Add(post);
             //save to db 
            await db.SaveChangesAsync();

            return Ok(HttpStatusCode.Created);
        }


        /*
         * Return Post with comments 
         */
        // GET: api/Posts/5
        [ResponseType(typeof(Post))]
        public async Task<IHttpActionResult> GetPost(int id)
        {
            Post post = await db.Posts
                .Include(e => e.Comments)
                .SingleOrDefaultAsync();
            if (post == null)
            {
                return NotFound();
            }

            return Ok(post);
        }


        /* 
         * Test Pass
         * POst Comment to the DB
         * Object reCived CommentDto
         *                                    
         * 
         * Also you can Factor out the Full Name and Send it with object from User Module
         */
        [Route("api/postComment")]
         [AuthorizeLevel(AuthorizationLevel.User)]
        public async Task<IHttpActionResult> postComment(CommentDto commentDto)
        {    
            if (commentDto.body.Length == 0)
            {
                return BadRequest("Post id or body is empty");
            }
            var user = User as ServiceUser;
            //User Id Return From User always have Custome: Prefix ,have to cut it out of string to get the Actual Id for Query 
            string userId = user.Id.Split(':')[1].Trim();
            int accountId = Convert.ToInt32(userId);
            Profile profile = await db.Profiles
                .Where(e => e.Account_Id == accountId)
                .SingleOrDefaultAsync();

            //Create fullNam efor comment
           String fullName=profile.FirstName + " " + profile.LastName;

               //Create comment in Db
           Comment comment = new Comment();
           comment.Name = fullName;
           comment.Body = commentDto.body;
           comment.PostId = commentDto.postId;
           comment.CreatedAt = DateTimeOffset.UtcNow;
           db.Comments.Add(comment);
           await db.SaveChangesAsync();

           return Ok(HttpStatusCode.Created); 
        }


        // PUT: api/Posts/5
      /*  [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPost(int id, Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != post.Id)
            {
                return BadRequest();
            }

            db.Entry(post).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

*/


        
        // DELETE: api/Posts/5
/*        [ResponseType(typeof(Post))]
        public async Task<IHttpActionResult> DeletePost(int id)
        {
            Post post = await db.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }

            db.Posts.Remove(post);
            await db.SaveChangesAsync();

            return Ok(post);
        }
*/
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PostExists(int id)
        {
            return db.Posts.Count(e => e.Id == id) > 0;
        }
    }
}