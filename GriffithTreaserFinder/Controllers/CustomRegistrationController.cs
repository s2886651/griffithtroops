﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using System.Text.RegularExpressions;
using GriffithTreaserFinder.Models;
using System.Threading.Tasks;
using GriffithTreaserFinder.Models.LoginModel;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace GriffithTreaserFinder.Controllers
{
   
    public class CustomRegistrationController : ApiController
    {
        GriffithDataModel context = new GriffithDataModel();
        public ApiServices Services { get; set; }

        // POST api/CustomRegistration
        public async Task<HttpResponseMessage> Post(RegistrationRequest registrationRequest)
        {
            if (!Regex.IsMatch(registrationRequest.email, @"^\w+([-+.']\w+)*@gmail.com$"))
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid username (Email has to be @gmail for now )");
            }
            else if (registrationRequest.password.Length < 8)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid password (at least 8 chars required)");
            }

            GriffithDataModel context = new GriffithDataModel();
            Account account = context.Accounts.Where(a => a.Email == registrationRequest.email).SingleOrDefault();
            if (account != null)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, "That username already exists.");
            }
            else
            {
                byte[] salt = CustomLoginProviderUtils.generateSalt();

                //random number to generate for checking Email criditional 
                Random random = new Random();
                Int16 randomChecker = (Int16)random.Next(10000);
                DateTimeOffset time = DateTimeOffset.Now;
  
                Account newAccount = new Account
                {          
                    Email = registrationRequest.email,
                    RandomCode=randomChecker,
                    EmailConfirmed=false,
                    Salt = salt,
                    SaltedAndHashedPassword = CustomLoginProviderUtils.hash(registrationRequest.password, salt),
                    CreatedAt=time
                };

                //Send email to Email ADDress
                EmailModel email=new EmailModel();
                  await email.createEmail(newAccount.Email, randomChecker.ToString());

                context.Accounts.Add(newAccount);
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
                return this.Request.CreateResponse(HttpStatusCode.Created,"Account created check your Email address for confirmation");
            }
        }

        /*
         * account confirmation with email link and bool valie in account 
         *Get: /api/sendConfirmation
         *
         */
        [Route("api/sendconfirmation")]
        public HttpResponseMessage GetconfirmEmail(string email,string random)
        { 
          Account account = context.Accounts.Where(e => e.Email == email).SingleOrDefault();
            //account confirmed
              if (account != null && account.RandomCode.ToString() == random)
              {
                  account.EmailConfirmed = true;
                  //create profile for accepted user 
                  Profile profile = new Profile() { 
                  Account_Id=account.Id,CreatedAt=System.DateTimeOffset.Now 
                  };
                  profile.Account = account;
                  account.Profiles.Add(profile);
                  context.Profiles.Add(profile);
                  context.SaveChanges();
                  return this.Request.CreateResponse(HttpStatusCode.Created);
   
          }
            return this.Request.CreateResponse(HttpStatusCode.BadRequest, "Can't find the account");

        }


        /*Method to send code again if user lost it
         *Post: "api/resendEmail"
         *Object body: ConfirmRegistor
         */
        [Route("api/resendEmail")]
        public async Task<HttpResponseMessage> PostEmailCodeRetrive(ConfirmRegister userEmail)
        {
            //Email is uniqe
            var account = context.Accounts.Where(e => e.Email == userEmail.Email);
           
           //Send email to Email ADDress
            if (account != null&& account.Count()==1)
            {
                Account ac = account.First();

                EmailModel email = new EmailModel();
               await email.createEmail(ac.Email, ac.RandomCode.ToString());
                return this.Request.CreateResponse(HttpStatusCode.OK);
            }
            return this.Request.CreateNotFoundResponse("Email:{0} not found", userEmail.Email);

        }

    }

}
