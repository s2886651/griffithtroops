﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using System.Security.Claims;
using GriffithTreaserFinder.Models;

namespace GriffithTreaserFinder.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class CustomLoginController : ApiController
    {
        public ApiServices Services { get; set; }
        public IServiceTokenHandler handler { get; set; }

        // POST api/CustomLogin
        //Header Variable : X-ZUMO-AUTH : AutToken 
        public HttpResponseMessage Post(LoginRequest loginRequest)
        {

            GriffithDataModel context = new GriffithDataModel();
            Account account = context.Accounts
                .Where(a => a.Email == loginRequest.email).SingleOrDefault();


            if (account != null)
            {
                byte[] incoming = CustomLoginProviderUtils
                    .hash(loginRequest.password, account.Salt);

                if (CustomLoginProviderUtils.slowEquals(incoming, account.SaltedAndHashedPassword))
                {
                    ClaimsIdentity claimsIdentity = new ClaimsIdentity();
                    //Add userId to the claimIdentity to use later in query 
                    claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier,account.Id.ToString()));
                
                    LoginResult loginResult = new CustomLoginProvider(handler)
                        .CreateLoginResult(claimsIdentity, Services.Settings.MasterKey);
                   var user=loginResult.User;
                    var customLoginResult = new CustomLoginResult()
                    {
                       
                        //original code to get Id back and its number Int 
                     //   UserId = loginResult.User.UserId,

                     //I want get the email address back fro accessToken storing in KeyChain 
                     UserId=loginRequest.email,
                        MobileServiceAuthenticationToken = loginResult.AuthenticationToken
                    };
                    return this.Request.CreateResponse(HttpStatusCode.OK, customLoginResult);
                }
            }
            return this.Request.CreateResponse(HttpStatusCode.Unauthorized,
                "Invalid username or password");
        }
    }
}
