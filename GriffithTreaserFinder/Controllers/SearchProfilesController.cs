﻿using GriffithTreaserFinder.DataObjects;
using GriffithTreaserFinder.Models;
using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GriffithTreaserFinder.Controllers
{
    public class SearchProfilesController : ApiController
    {

        private GriffithDataModel db = new GriffithDataModel();
        public ApiServices Services { get; set; } //need ApiSErvice To work on Mobile services

        /*
         * 
         */

        public IHttpActionResult getProfiles(ProfileSearchDto profileSearch) {


            List<Profile> profiles=new List<Profile>();

            if (profileSearch.interestedSex.Length != 0)
            {
                if (profileSearch.major.Length != 0)
                {

                    profiles = db.Profiles.Where(e => e.Sex == profileSearch.interestedSex)
                           .Where(q => q.Major == profileSearch.major).ToList();
                }
                else
                    if (profileSearch.campus.Length != 0)
                    {

                        profiles = db.Profiles.Where(e => e.Sex == profileSearch.interestedSex)
                               .Where(t => t.Campus == profileSearch.campus).ToList();
                    }
                    else
                        if (profileSearch.major.Length != 0 && profileSearch.campus.Length != 0)
                        {

                            int i = db.Profiles.Where(e => e.Sex == profileSearch.interestedSex)
                                   .Where(t => t.Campus == profileSearch.campus)
                                   .Where(q => q.Major == profileSearch.major)
                                   .Count();
                            if (i > 0) { //Check if cant find profile with these 2 Attribute

                                profiles = db.Profiles.Where(e => e.Sex == profileSearch.interestedSex)
                                            .Where(t => t.Campus == profileSearch.campus)
                                            .Where(q => q.Major == profileSearch.major).ToList();
                            } else { //Going to exist with Campus so check only campus 

                                profiles = db.Profiles
                                    .Where(e => e.Campus == profileSearch.campus).ToList();
                            }
                        }

            return Ok(profiles);
            }//intersetSex check 
            else {

                HttpResponseMessage response = new HttpResponseMessage();
                response.ReasonPhrase = "cant find without the sexIntereset ";
                response.StatusCode = HttpStatusCode.BadRequest;
                return ResponseMessage(response);
            }

        
        }
    }
}
