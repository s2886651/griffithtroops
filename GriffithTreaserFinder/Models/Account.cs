namespace GriffithTreaserFinder.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Account
    {
        public Account()
        {
         //   Profiles = new HashSet<Profile>();
        }

        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        [Required]
        public byte[] Salt { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Required]
        public byte[] SaltedAndHashedPassword { get; set; }

        public short RandomCode { get; set; }

        public virtual ICollection<Profile> Profiles { get; set; }
    }
}
