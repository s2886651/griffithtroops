﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.Models
{
    public partial class Post
    {
        public Post()
        {
          
        }

        public int Id { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string Title { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public int AccountId { get; set; }

        public virtual Account Account { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }


}