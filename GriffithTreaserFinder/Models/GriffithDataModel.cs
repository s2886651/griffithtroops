namespace GriffithTreaserFinder.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class GriffithDataModel : DbContext
    {
        public GriffithDataModel()
            : base("name=DataModel")
        {
             base.Configuration.LazyLoadingEnabled = false; 
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<UserImage> UserImages { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasMany(e => e.Profiles)
                .WithRequired(e => e.Account)
                .HasForeignKey(e => e.Account_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>()
                .HasMany(e => e.UserImages)
                .WithRequired(e => e.Profile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Profiles)
                .WithRequired(e => e.Account)
                .HasForeignKey(e => e.Account_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Post>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Post)
                .WillCascadeOnDelete(false);
        }
    }
}
