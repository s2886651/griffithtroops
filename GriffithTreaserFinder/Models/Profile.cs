namespace GriffithTreaserFinder.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Profile
    {
        public Profile()
        {
         //   UserImages = new HashSet<UserImage>();
        }

        public int Id { get; set; }

        public string FirstName { get; set; }
     
        public string MiddleName { get; set; }
  
        public string LastName { get; set; }
      
        public string Major { get; set; }
        public string Sex { get; set; }
        public string Campus { get; set; }

        public DateTime? EntryYear { get; set; }

        public DateTime? BirthDay { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdateAt { get; set; }

        public int Account_Id { get; set; }

        public virtual Account Account { get; set; }

        public virtual ICollection<UserImage> UserImages { get; set; }
    }
}
