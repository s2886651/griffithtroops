﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.Models.BlobStorage
{
    public class TableStorageHandler
    {


        public void getTable(){

      
      CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                                                      CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the table if it doesn't exist.
            CloudTable table = tableClient.GetTableReference("userposts");
            table.CreateIfNotExists();

        }
    }
}