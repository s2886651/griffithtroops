﻿using GriffithTreaserFinder.DataObjects;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace GriffithTreaserFinder.Models.BlobStorage
{
    public class BlobStorageCustomHandler
    {
        /*
         * Method to create container And generate SAS(Share Access Signature 
         * Create User Image Object and returning it 
         */
        public async Task<UserImage> returnImage(UserImageDto item)
        {

            // Try to get the Azure storage account token from app settings.  
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                                                      CloudConfigurationManager.GetSetting("StorageConnectionString"));
            Uri blobEndpoint = new Uri(string.Format("https://{0}.blob.core.windows.net", storageAccount.Credentials.AccountName));
            // Create the BLOB service client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference(item.ContainerName);
                await container.CreateIfNotExistsAsync();
                // Create a shared access permission policy. 
                BlobContainerPermissions containerPermissions = new BlobContainerPermissions();
                // Enable anonymous read access to BLOBs.
                containerPermissions.PublicAccess = BlobContainerPublicAccessType.Blob;
                container.SetPermissions(containerPermissions);
                // Define a policy that gives write access to the container for 5 minutes.                                   
                SharedAccessBlobPolicy sasPolicy = new SharedAccessBlobPolicy()
                {
                    SharedAccessStartTime = DateTime.UtcNow,
                    SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24),
                    Permissions = SharedAccessBlobPermissions.Write
                };
            UserImage image=new UserImage();
            image.ContainerName = item.ContainerName;
            image.ResourcesName = item.ResourcesName;
                // Get the SAS as a string.
                image.SasQueryString = container.GetSharedAccessSignature(sasPolicy);
                // Set the URL used to store the image.
                image.imageUri = string.Format("{0}{1}/{2}", blobEndpoint.ToString(),
                    item.ContainerName, item.ResourcesName);
                return image;
        }
    }
}