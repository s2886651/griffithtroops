﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.Models
{

    //Anything you want catch from user should be here in this class
    public class RegistrationRequest
    {
        public String email { get; set; }
        public String password { get; set; }
    }
}