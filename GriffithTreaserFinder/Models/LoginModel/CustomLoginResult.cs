﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/*Object returns when user login 
 properties userId and accessToken Key 
 */
namespace GriffithTreaserFinder.Models
{
    public class CustomLoginResult
    {
        public string UserId { get; set; }
        public string MobileServiceAuthenticationToken { get; set; }
    }
}