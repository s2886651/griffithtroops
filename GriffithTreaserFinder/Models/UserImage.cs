namespace GriffithTreaserFinder.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserImage
    {
        public int Id { get; set; }

        [Required]
        public string ContainerName { get; set; }

        [Required]
        public string ResourcesName { get; set; }

        [Required]
        public string SasQueryString { get; set; }

        public bool? ProfilePic { get; set; }

        public string imageUri { get; set; } 

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        public int ProfileId { get; set; }

        public virtual Profile Profile { get; set; }
    }
}
