﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.Models
{
    public partial class Comment
    {
        public int Id { get; set; }

        [Required]
        public string Body { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public int PostId { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual Post Post { get; set; }
    }

}