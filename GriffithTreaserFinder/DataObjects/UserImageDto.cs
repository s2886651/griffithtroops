﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.DataObjects
{
    public class UserImageDto
    {

        public string ContainerName { get; set; }
        public string ResourcesName { get; set; }
    }
}