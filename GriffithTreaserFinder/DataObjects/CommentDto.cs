﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.DataObjects
{
    public class CommentDto
    {
        public int postId { get; set; }
        public string body { get; set; }
  
    }
}