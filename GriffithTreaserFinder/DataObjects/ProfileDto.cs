﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.DataObjects
{
    public class ProfileDto
    {

        public int? profileId { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }
        public string Sex { get; set; }

        public string Major { get; set; }
        public string Campus { get; set; }

        public DateTime? EntryYear { get; set; }

        public DateTime? BirthDay { get; set; }

    }
}