﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GriffithTreaserFinder.DataObjects
{
    public class BlobImage
    {
        public string  UtcDate {get;set;}
        public string Sas { get; set; }
    }
}