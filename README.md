# README #

#Griffith Tinder Style Project Back-end C# 

This project Idea comes from student in Multimedia school of Griffith, they pointed out that lots of students using Griffith crushes(Facebook page) at the moment and going to be really helpful if someone develop system like Tinder to replace the Facebook group and make it easier to use for this particular purpose.
  The Project created and handled by me (Abbas Safaie) in backend and IOS module to create full stack system over the network and create social media tinder style app for Griffith students.
 For back End part of this Project I’m using Web API in ASP.Net framework and handle data in relational matter by leveraging  Azure SQL (Similar to T-SQL) as a database for tuples and using Azure Storage for storing Key value and heap Data such as Blob (Images).


### **Login and Registration:** ###
For purpose of Auth  I’m using Owin and OAuth2 to create custom login and registration to allow only students with Griffith Email account to join the system and register(I’m not using build in login system in Asp.net).

### **Blob Storage:** ###
For storing Images over the network for small cost, I decided to use Azure Blob storage.
By using Azure storage Blob I’m sending SAS to Authorized user and give permission to upload image in next 15 minutes from same device.
Object Mapping:
For this project I’m having 2 type of object DTO(Data Transfer Objects) and Model Object .
Model objects have a responsibility to handle the data from and to SQL and create Objects from return tuples.
DTO objects are acting like a view in my system as they have responsibility to  transfer necessary data to user and capture data from user by specific format and to prevent the system to expose the critical data over the network such as database tuples ID’s.

Sprint 2
Going to extend the system and add the new controller to handle Matching algorithm for my system, basically just getting information from user as there interested sex, campus and major and match them up for closes candidates, (SearchProfileController), in progress at the moment .